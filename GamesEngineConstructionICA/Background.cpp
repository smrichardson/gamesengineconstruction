#pragma once
#include "Visualisation.h"
#include "Background.h"
#include "Audio.h"

CBackground::CBackground()
{
}

CBackground::~CBackground()
{

}

CBackground::CBackground(int x, int y) : CWorldEntity(x, y, BACKGROUND)
{
	// disregard x and y pos for background as always full screen
	CVisualisation::Instance()->CreateSprite("space.png", 1024, 512, 0, 0);
	SetVisible(true);

	EntityRect = CRectangle(0, 512, 0, 1024);

	// play the audio stream
	// audio file from http://www.bensound.com/
	CAudio::Instance()->PlayStream("bensound-extremeaction.mp3");
}

void CBackground::Update(float deltaTime, float startTime)
{

	decrementX(_backgroundIncrement, deltaTime, startTime);
	if (_xOffset >= 1024)
	{
		_xOffset = 0;
	}
	_xOffset = _xOffset+_backgroundIncrement;
	if (GetX() + 1024 <= 0)
	{
		_posX = 1024 - _xOffset;
	}
}

void CBackground::Render()
{
	// image always id 0 for background
	CVisualisation::Instance()->DrawBackground(0, _posX, _posY);
	CVisualisation::Instance()->DrawBackground(0, 1024 - _xOffset, _posY);
}

void CBackground::Move(Direction direction, float deltaTime, float startTime)
{
	
}

void CBackground::MoveBackground(int x, int y)
{
	_posX = x;
	_posY = y;

	SetVisible(true);
}

bool CBackground::CheckForCollision(CWorldEntity* otherEntity)
{
	return false;
}