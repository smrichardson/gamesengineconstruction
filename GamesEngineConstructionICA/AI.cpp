#include "AI.h"

CAI CAI::_AI;

CAI::CAI()
{
}


CAI::~CAI()
{
}

//code for some AI logic
void CAI::AILogic(CWorldEntity* entity, CWorldEntity* otherEntity, float deltaTime, float startTime)
{
	int playerX = entity->GetX();
	int playerY = entity->GetY();
	int enemyX = otherEntity->GetX();
	int enemyY = otherEntity->GetY();

	_distX = playerX - enemyX;
	_distY = playerY - enemyY;

	double hypDistSquared = pow((_distX), 2) + pow((_distY), 2);
	_hypDist = sqrt(hypDistSquared);

	_angleX = (acos(_distX / _hypDist) * 180 / PI);
	_angleY = (acos(_distY / _hypDist) * 180 / PI);
	
	// this can be modified to move the AI differently so it would be for more enemy types
	if (_angleX >= 90 && _angleX <= 180)
	{
		otherEntity->Move(LEFT, deltaTime, startTime);
	}
	else if (_angleX >= 0 && _angleX <= 90)
	{
		otherEntity->Move(LEFT, deltaTime, startTime);
	}
	if (_angleY > 90 && _angleY < 180)
	{
		otherEntity->Move(LEFT, deltaTime, startTime);
	}
	else if (_angleY > 0 && _angleY < 90)
	{
		otherEntity->Move(LEFT, deltaTime, startTime);
	}
}