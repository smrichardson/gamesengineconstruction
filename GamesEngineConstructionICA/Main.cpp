#pragma once
#ifndef MAIN
#define MAIN
#define GAMETICK 30

#include <HAPI_lib.h>
#include "Rectangle.h"
#include <time.h>
#include "Visualisation.h"
#include "Utils.h"
#include "WorldModel.h"
#include <algorithm>

void HAPI_Main()
{
	// wait for timeout
	bool waitFlag = true; 

	// create the world
	CWorldModel* world = new CWorldModel();

	// initialise the world
	world->Initialise();

	// display the menu

	// process menu option

	// temp set to play
	world->SetGameState(PLAY);

	// loop while playing
	while (world->GetGameState() == PLAY)
	{
		//work out the deltaTime for the loop
		world->SetFrameStartTime(HAPI->GetTime());
		world->SetFrameEndTime(HAPI->GetTime());
		world->SetDeltaTime((float)(world->GetFrameEndTime() - world->GetFrameStartTime()));
		// handle inputs
		world->HandleEvents();

		//loop only every gametick
		while (waitFlag)
		{
			// get the start time for the loop
			if (HAPI->GetTime() - world->GetFrameStartTime() > GAMETICK)
			{
				// update the game state
				world->Update(world->GetDeltaTime());
				waitFlag = false;
			}
		}

		// reset wait flag
		waitFlag = true;

		// render everthing
		world->Render();
	}

	//loop while finished
	while (world->GetGameState() == GAME_OVER)
	{
		// update the game state
		world->Update(world->GetDeltaTime());
	}
	// clean up everything
	world->Cleanup();
	delete world;
}
#endif