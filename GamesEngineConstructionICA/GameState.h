#pragma once
#ifndef GAMESTATE_H
#define GAMESTATE_H
#include <HAPI_lib.h>

class GameEngine;

class GameState
{
public:
	virtual void Init() = 0;
	virtual void Cleanup() = 0;

	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual void HandleEvents() = 0;
	virtual void Update(int deltaTime) = 0;
	virtual void Render() = 0;
	
	
	void ChangeState(GameEngine* game, GameState* state);
protected:
	GameState(){}
};

#endif