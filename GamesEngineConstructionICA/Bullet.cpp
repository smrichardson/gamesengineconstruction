#include "Bullet.h"
#include "Visualisation.h"
#include "Audio.h"

CBullet::CBullet() 
{
}

CBullet::CBullet(int x, int y) : CWorldEntity(x, y, BULLET)
{
	_health = 1;
	CVisualisation::Instance()->CreateSprite("playerBullet.png", 16, 4, 0, 0);
	SetVisible(false);

	EntityRect = CRectangle(0, 4, 0, 16);
	// load fire sound
	CAudio::Instance()->CreateAudio("weaponfire10.wav");
}

CBullet::~CBullet()
{
}

void CBullet::Update(float deltaTime, float startTime)
{
	if (IsVisible())
	{
		incrementX(_bulletIncrement, deltaTime, startTime);
	}
	if (_posX > CVisualisation::Instance()->ScreenWidth())
	{
		_visible = false;
		CAudio::Instance()->StopPlay(0);
	}
}

void CBullet::Render()
{
	// only render the sprite if visible
	if (IsVisible())
	{
		CVisualisation::Instance()->DrawSprite(10, _posX, _posY, 0);
	}
}

void CBullet::Move(Direction direction, float deltaTime, float startTime)
{

}

void CBullet::Fire(int x, int y)
{
	// set the initial position of the bullet
	_posX = x;
	_posY = y;
	
	// make it visible
	SetVisible(true);

	// play its sound
	CAudio::Instance()->Play(0, false, 1000, 500);
}

bool CBullet::CheckForCollision(CWorldEntity* otherEntity)
{
	if (IsVisible() && otherEntity->IsVisible())
	{
		if (otherEntity->GetEntityType() == ENEMY)
		{
			if ((GetX() + EntityRect.right > otherEntity->GetX() + otherEntity->GetRect().left)
				&& (GetX() < otherEntity->GetX() + otherEntity->GetRect().right)
				&& (GetY() + EntityRect.bottom > otherEntity->GetY() + otherEntity->GetRect().top)
				&& (GetY() < otherEntity->GetY() + otherEntity->GetRect().bottom))
			{
				SetVisible(false);
				CAudio::Instance()->StopPlay(0);
 				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	return false;
}