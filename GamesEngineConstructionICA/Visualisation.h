#pragma once
#ifndef VISUALISATION_H
#define VISUALISATION_H
#include <HAPI_lib.h>
#include <vector>

class CSprite;
class Player;

class CVisualisation
{
public:
	// Default constructor
	CVisualisation();

	// Destructor
	~CVisualisation();
	
	// Class initialiser
	void Init();
	
	// Create a sprite instance
	//
	// parameters:
	// filename - the name of the texture file
	// width - the width of an animation frame
	// height - the height of an animation frame
	// row - the row within the texture file that the sprite image is located at
	// col - the column within the texture file that the sprite image is located at
	// id - the id of the sprite
	//
	// returns : an integer representation of the sprite
	//
	int CreateSprite(std::string const& filename, int width, int height, int row, int col);

	// Draws a sprite instance
	//
	// parameters:
	// spriteId - the id of the sprite to draw
	// x - the x position on the screen to draw the sprite
	// y - the y position on the screen to draw the sprite
	// frameNumber - the frame of the animation to draw
	void DrawSprite(int spriteId, int x, int y, int frameNumber);

	// Draws a sprite instance
	//
	// parameters:
	// spriteId - the id of the sprite to draw
	// x - the x position on the screen to draw the sprite
	// y - the y position on the screen to draw the sprite
	void DrawBackground(int spriteId, int x, int y);

	// Clears the screen
	void ClearScreen();

	// returns the width of the screen
	int ScreenWidth() { return _screenWidth; }

	// returns the height of the screen
	int ScreenHeight() { return _screenHeight; }

	// returns the screen
	BYTE* Screen() { return _screen; }

	// get the class instance
	static CVisualisation* Instance()
	{
		return &_visualisation;
	}

private:

	// screen size in pixels
	int _screenWidth = 1024;
	int _screenHeight = 512;

	// the screen
	BYTE *_screen = 0;

	// sprite instances
	std::vector<CSprite*> _sprites;

	// the variable to hold the instance of this class
	static CVisualisation _visualisation;
};

#endif