#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "HAPI_lib.h"
#include "WorldEntity.h"
#include "Bullet.h"
#include "Rectangle.h"

class GameEngine;

class CPlayer : public CWorldEntity
{
public:

	// Default constructor
	CPlayer();

	// Constructor
	CPlayer(int x, int y);

	// Desctructor
	~CPlayer();

	// handle update of entity
	void Update(float deltaTime, float startTime);

	// handle render of entity
	void Render();

	// handle movement of player
	void Move(Direction direction, float deltaTime, float startTime);

	bool CheckForCollision(CWorldEntity *otherEntity);

	void Fire();

private:
	int _playerIncrement = 5;
	// constants for animation
	const int IDLE = 5;
	const int START_MOVE_UP = 6;
	const int END_MOVE_UP = 9;
	const int START_MOVE_DOWN = 5;
	const int END_MOVE_DOWN = 1;
	const int ANIMATION_FRAME_CORRECTION = 6;
};
#endif

