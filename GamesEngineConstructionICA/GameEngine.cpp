#include "GameEngine.h"
#include "GameState.h"
#include "Utils.h"

GameEngine GameEngine::_gameEngine;

GameEngine::GameEngine()
{
}


GameEngine::~GameEngine()
{
}

void GameEngine::Init()
{
	_player = new CPlayer(0, 321);
	SetRunning(true);
}

void GameEngine::HandleEvents()
{
	//_states.back()->HandleEvents(this);
}

void GameEngine::Cleanup()
{
	HAPI->Close();
}

void GameEngine::Update(int deltaTime)
{
	HAPI->Update();
	//_states.back()->Update(this, deltaTime);
}

void GameEngine::Render()
{
	//_states.back()->Render(this);
}

void GameEngine::ChangeState(GameState* state)
{
	// cleanup the current state
	if (!_states.empty()) {
		_states.back()->Cleanup();
		_states.pop_back();
	}

	// store and init the new state
	_states.push_back(state);
	_states.back()->Init();
}