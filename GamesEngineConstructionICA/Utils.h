#include "HAPI_lib.h"
#include "Rectangle.h"

namespace Utils
{
	// Blit an image  onto the screen
	//
	// parameters:
	//
	// destination - the screen that the image is displayed
	// destWidth - the width of the screen
	// source - the image data file
	// sourceWidth - the width of the image
	// sourceHeight - the height of the image
	// posX - the x position of the image
	// posY - the y postion of the image
	//
	void Blit(BYTE* destination, int destWidth, BYTE* source, int sourceWidth, int sourceHeight, int posX, int posY);

	// The image which is blitted to the screen clips against the side
	//
	// parameters:
	//
	// destination - the screen that the image is displayed
	// destRect - the rectangle of the screen
	// source - the image data file
	// sourceRect - the rectangle of the image
	// posX - the x position of the image
	// posY - the y postion of the image
	//
	void ClipBlit(BYTE* destination, const CRectangle &destRect, BYTE* source, const CRectangle &sourceRect, int posX, int posY);

	// the image which is blitted is a fraction of the actual image
	//
	// parameters:
	// 
	// destination - the screen that the image is displayed
	// destRect - the rectangle of the screen
	// source - the image data file
	// sourceRect - the rectangle of the image
	// posX - the x position of the image
	// posY - the y postion of the image
	// row - the row which the fraction of the image shows
	// col - the col which the fraction of the image shows
	// spriteWidth - the width of the sprite
	// spriteHeight - the height of the sprite
	//
	void CropBlit(BYTE* destination, const CRectangle &destRect, BYTE* source, const CRectangle &sourceRect, int posX, int posY, int row, int col, int spriteWidth, int spriteHeight);
	
	// the image blitted has aplpha values
	// parameters:
	//
	// destination - the screen that the image is displayed
	// destWidth - the width of the screen
	// source - the image data file
	// sourceWidth - the width of the image
	// sourceHeight - the height of the image
	// posX - the x position of the image
	// posY - the y postion of the image
	//
	void transparentBlit(BYTE* destination, int destWidth, BYTE* source, int sourceWidth, int sourceHeight, int posX, int posY);
	
	// A function for linear interpolation between two points
	//
	// parameters:
	//
	// firstPos - the first point of the of an entity
	// newPos - the point the entity moves to
	// deltaTime - delta time of the game loop
	// startTime - start time of the loop
	//
	float Lerp(int firstPos, int newPos, float deltaTime, float startTime);
}