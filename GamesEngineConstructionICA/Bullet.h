#pragma once
#include"WorldEntity.h"

class CBullet : public CWorldEntity
{
public:
	CBullet();
	CBullet(int x, int y);
	~CBullet();

	void Update(float deltaTime, float startTime);
	void Render();
	void Fire(int x, int y);
	void Move(Direction direction, float deltaTime, float startTime);
	bool CheckForCollision(CWorldEntity *otherEntity);

private:
	int _bulletIncrement = 25/2;
};

