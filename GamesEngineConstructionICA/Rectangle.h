#pragma once
class CRectangle
{
public:
	// the dimensions of the rectangle
	int top;
	int bottom;
	int left;
	int right;
	
	// create a rectangle
	CRectangle(int t, int b, int l, int r) : top(t), bottom(b), left(l), right(r){}
	
	// width and height of the rectangle
	int Width() const { return right - left; }
	int Height() const { return bottom - top; }

	//a function to make the source rectangle clip over the edges of the destination rectangle
	void ClipTo(const CRectangle &other, int tOffset, int bOffset, int lOffset, int rOffset)
	{
		//checks if a side of a rectangle is passed the same side of another rectangle 
		if (left - lOffset < other.left)
		{
			left = other.left + lOffset;
		}
		if (right - lOffset > other.right)
		{
			right = other.right + lOffset;
		}
		if (top - tOffset < other.top)
		{
			top = other.top + tOffset;
		}
		if (bottom - tOffset > other.bottom)
		{
			bottom = other.bottom + tOffset;
		}
	}

	//a function which translates the sides of a rectangle
	void Translate(int dX, int dY)
	{
		left += dX;
		right += dX;
		top += dY;
		bottom += dY;
	}

	//a function which tests if a source rectangle is outside of the destination rectangle 
	bool IsOutside(int posX, int posY, const CRectangle &destRect, const CRectangle &other)
	{
		// position is to the right of the destination rectangle
		if (posX > destRect.right)
			return true;
		else
			return false;
		// position is below the destination rectangle
		if (posY>destRect.bottom)
			return true;
		else
			return false;
		// position minus the sprite width is completely to the left 
		if (posX<destRect.left - other.Width())
			return true;
		else
			return false;
		// position minus the source height is completely above 
		if (posY < destRect.top - other.Height())
			return true;
		else
			return false;
	}

};

