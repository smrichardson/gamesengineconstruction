#pragma once
#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "HAPI_lib.h"
#include "WorldEntity.h"

class CBackground : public CWorldEntity
{
public:

	// Default constructor
	CBackground();

	// Constructor
	CBackground(int x, int y);

	// Desctructor
	~CBackground();

	// handle render of entity
	void Render();

	// handle update --- not required for background
	void Update(float deltaTime, float startTime);

	// handle move --- not required for background
	void Move(Direction direction, float deltaTime, float startTime);

	// handle check for collision -- not required for background
	bool CWorldEntity::CheckForCollision(CWorldEntity* otherEntity);

	// move the background
	void CBackground::MoveBackground(int x, int y);
private:
	int _xOffset = 0; // offset the background so it can scroll
	int _backgroundIncrement = 25; // how much to move the background across the screen by
};
#endif

