#ifndef PLAYSTATE_H
#define PLAYSTATE_H
#include "GameState.h"

class GameEngine;
class Player;
class Sprite;

class PlayState : public GameState
{
public:	
	void Init();

	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents() = 0;
	void Update(int deltaTime) = 0;
	void Render() = 0;

	/*static PlayState* Instance()
	{
		return &_InitState;
	}*/
protected:
	PlayState(){}
	
private:
	//static PlayState _InitState;
	
};
#endif

