#include "GameState.h"
#include "GameEngine.h"

void GameState::Init()
{
	
}

void GameState::Cleanup()
{

}

void GameState::Update(int deltaTime)
{
	
}

void GameState::Render()
{
	
	
}

void ChangeState(GameEngine* game, GameState* state)
{
	game->ChangeState(state);
}
