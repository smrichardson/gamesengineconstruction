#pragma once
#include "Visualisation.h"
#include"Audio.h"
#include "Player.h"
#include "Background.h"
#include "Enemy.h"
#include "Explosion.h"
#include "AI.h"

// directions the entity can travel
enum GameState {
	INIT = 0,
	MENU,
	PLAY,
	GAME_OVER
};

class CWorldModel
{
public:
	CWorldModel();
	~CWorldModel();

	// setter for running
	void SetRunning(const bool& value) { _running = value; }

	// getter for running
	bool IsRunning() { return _running; }

	// getter for player
	CPlayer* GetPlayer() { return (CPlayer*)(_entities[1]); }

	// getter for enemy
	CExplosion* GetExplosion() { return (CExplosion*)_entities[44]; }

	// getter for bullet
	CBullet* GetBullet() { return (CBullet*)(_entities[2]); }

	CBackground* GetBackground() { return (CBackground*)_entities[0]; }

	// setter for controller present
	void SetControllerPresent(const bool & value) { _controllerPresent = value; }

	// getter for controller present
	bool IsControllerPresent() { return _controllerPresent;  }

	// getter for keyData
	HAPI_TKeyboardData GetKeyData() { return _keyData; }

	// getter for controller data
	HAPI_TControllerData GetControllerData() { return _controllerData; }

	// getters for CTRLMax/Min
	const int CTRLMax() { return _ctrlMax; }
	const int CTRLMin() { return _ctrlMin; }

	// getter/setter for game state
	GameState GetGameState() { return _currentState; }
	void SetGameState(GameState value) { _currentState = value; }

	// initialiser for the world
	void Initialise();

	// handle world cleanup
	void Cleanup();

	void HandleEvents();
	void Update(float deltaTime);
	void Render();

	void AILogic(CWorldEntity* player, CWorldEntity* enemy);

	// timing related getters/setters
	float GetDeltaTime() { return _deltaTime; }
	void SetDeltaTime(float value) { _deltaTime = value; }
	void SetFrameStartTime(const DWORD& value) { _frameStartTime = value; }
	void SetFrameEndTime(const DWORD& value) { _frameEndTime = value; }
	DWORD GetFrameStartTime() { return _frameStartTime; }
	DWORD GetFrameEndTime() { return _frameEndTime; }
	int GetFrameMs() { return FRAME_MS; }



private:

	// define structures to hold keyboard and controller data
	HAPI_TKeyboardData _keyData;
	HAPI_TControllerData _controllerData;

	//max values for the controller analogue sticks
	const int _ctrlMax = 32767;
	const int _ctrlMin = -32767;

	// flag to denote in game loop
	bool _running = false;

	// flag to denote game controller plugged in
	bool _controllerPresent = false;

	// vector of entities
	std::vector<CWorldEntity*> _entities;

	// the current state of the game
	GameState _currentState = GameState::INIT;

	// timing data
	float _deltaTime = 0;
	DWORD _frameStartTime = 0;
	DWORD _frameEndTime = 0;

	// frame rate constants
	const int FRAMES_PER_SECOND = 60;
	const int FRAME_MS = 1000 / FRAMES_PER_SECOND;

	//offset for the enemy positions
	int _xOffset;
	int _yOffset;

	//score of the player
	int _score;
};

