#include "Utils.h"

namespace Utils
{
	void Blit(BYTE *destination, int destWidth, BYTE *source, int sourceWidth, int sourceHeight, int posX, int posY)
	{
		BYTE *destPnter = destination + (posX + posY*destWidth) * 4;
		BYTE *sourcePnter = source;

		for (int y = 0; y < sourceHeight; y++)
		{
			memcpy(destPnter, sourcePnter, sourceWidth * 4);
			// Move source pointer to next line
			sourcePnter += sourceWidth * 4;

			// Move destination pointer to next line
			destPnter += destWidth * 4;
		}
	}
	//a function which blits a source rectangle with an alpha blended image of the same size
	void ClipBlit(BYTE *destination, const CRectangle &destRect, BYTE *source, const CRectangle &sourceRect, int posX, int posY)
	{

		CRectangle ClippedRect(sourceRect);
		if (ClippedRect.IsOutside(posX, posY, destRect, sourceRect))
			return;

		if (!ClippedRect.IsOutside(posX, posY, destRect, sourceRect)) {
			//translate to screen space
			ClippedRect.Translate(posX, posY);
			//clip it
			ClippedRect.ClipTo(destRect, 0, 0, 0, 0);
			//translate back to source space
			ClippedRect.Translate(-posX, -posY);
			//clamp the image at negative values
			if (posX <= 0)
			{
				posX = 0;
			}
			if (posY <= 0)
			{
				posY = 0;
			}
		}
		//move the pointers to the next line
		BYTE *destPnter = destination + (posX + posY * destRect.Width()) * 4;
		BYTE *sourcePnter = source + (ClippedRect.top*sourceRect.Width() + ClippedRect.left) * 4;

		// blit code
		for (int y = 0; y < ClippedRect.Height(); y++)
		{
			for (int x = 0; x < ClippedRect.Width(); x++)
			{
				BYTE alpha = sourcePnter[3];

				if (0 == alpha)
				{
					//do nothing
				}
				else if (255 == alpha)
				{
					memcpy(destPnter, sourcePnter, 4);
				}
				else
				{
					float mod = alpha / 255.0f;

					BYTE blue = sourcePnter[0];
					BYTE green = sourcePnter[1];
					BYTE red = sourcePnter[2];

					destPnter[0] = destPnter[0] + ((alpha*(blue - destPnter[0])) >> 8);
					destPnter[1] = destPnter[1] + ((alpha*(green - destPnter[1])) >> 8);
					destPnter[2] = destPnter[2] + ((alpha*(red - destPnter[2])) >> 8);

				}
				destPnter += 4;
				sourcePnter += 4;
			}
			destPnter += (destRect.Width() - ClippedRect.Width()) * 4;
			sourcePnter += (sourceRect.Width() - ClippedRect.Width()) * 4;
		}
		ClippedRect.left += sourceRect.left;
		ClippedRect.right += sourceRect.left;
		ClippedRect.top += sourceRect.top;
		ClippedRect.bottom += sourceRect.top;
	}

	void CropBlit(BYTE *destination, const CRectangle &destRect, BYTE *source, const CRectangle &sourceRect, int posX, int posY, int row, int col, int spriteWidth, int spriteHeight) 
	{
		int top = row * spriteHeight;
		int bottom = top + spriteHeight;
		int left = col * spriteWidth;
		int right = left + spriteWidth;
		CRectangle ClippedRect(top,bottom,left,right);
		if (ClippedRect.IsOutside(posX, posY, destRect, sourceRect))
			return;

		if (!ClippedRect.IsOutside(posX, posY, destRect, sourceRect)) {
			//translate to screen space
			ClippedRect.Translate(posX, posY);
			//clip it
			ClippedRect.ClipTo(destRect, top, bottom, left, right);
			//translate back to source space
			ClippedRect.Translate(-posX, -posY);
			//clamp the image at negative values
			if (posX <= 0)
			{
				posX = 0;
			}
			if (posY <= 0)
			{
				posY = 0;
			}
		}
		//move the pointers to the next line
		BYTE *destPnter = destination + (posX + posY * destRect.Width()) * 4;
		BYTE *sourcePnter = source + (ClippedRect.top*sourceRect.Width() + ClippedRect.left) * 4;

		// blit code
		for (int y = 0; y < ClippedRect.Height(); y++)
		{
			for (int x = 0; x < ClippedRect.Width(); x++)
			{
				BYTE alpha = sourcePnter[3];

				if (0 == alpha)
				{
					//do nothing
				}
				else if (255 == alpha)
				{
					memcpy(destPnter, sourcePnter, 4);
				}
				else
				{
					float mod = alpha / 255.0f;

					BYTE blue = sourcePnter[0];
					BYTE green = sourcePnter[1];
					BYTE red = sourcePnter[2];

					destPnter[0] = destPnter[0] + ((alpha*(blue - destPnter[0])) >> 8);
					destPnter[1] = destPnter[1] + ((alpha*(green - destPnter[1])) >> 8);
					destPnter[2] = destPnter[2] + ((alpha*(red - destPnter[2])) >> 8);

				}
				destPnter += 4;
				sourcePnter += 4;
			}
			destPnter += (destRect.Width() - ClippedRect.Width()) * 4;
			sourcePnter += (sourceRect.Width() - ClippedRect.Width()) * 4;
		}
		ClippedRect.left += sourceRect.left;
		ClippedRect.right += sourceRect.left;
		ClippedRect.top += sourceRect.top;
		ClippedRect.bottom += sourceRect.top;
	}


	void transparentBlit(BYTE *destination, int destWidth, BYTE *source, int sourceWidth, int sourceHeight, int posX, int posY)
	{
		BYTE *destPnter = destination + (posX + posY*destWidth) * 4;
		BYTE *sourcePnter = source;

		for (int y = 0; y < sourceHeight; y++)
		{
			for (int x = 0; x < sourceWidth; x++)
			{
				BYTE alpha = sourcePnter[3];

				if (0 == alpha)
				{
					//do nothing
				}
				else if (255 == alpha)
				{
					memcpy(destPnter, sourcePnter, 4);
				}
				else
				{
					float mod = alpha / 255.0f;

					BYTE blue = sourcePnter[0];
					BYTE green = sourcePnter[1];
					BYTE red = sourcePnter[2];

					destPnter[0] = (BYTE)(mod*blue + (1.0f - mod)*destPnter[0]);
					destPnter[1] = (BYTE)(mod*green + (1.0f - mod) * destPnter[1]);
					destPnter[2] = (BYTE)(mod*red + (1.0f - mod) * destPnter[2]);
				}
				destPnter += 4;
				sourcePnter += 4;
			}
			destPnter += (destWidth - sourceWidth) * 4;
		}
	}

	// a function which moves an entity using linear interpolation
	float Lerp(int firstPos, int newPos, float deltaTime, float startTime)
	{
		DWORD t = HAPI->GetTime();
		float s = (t - startTime) / deltaTime;
		//return firstPos * (1 - s) + newPos * s;
		return firstPos + s*(newPos - firstPos);
	}

}