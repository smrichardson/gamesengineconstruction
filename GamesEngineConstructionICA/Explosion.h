#pragma once
#include"WorldEntity.h"

class CExplosion : public CWorldEntity
{
public:
	//default constructor
	CExplosion();

	//constructor with the positions of the explosion on the screen
	CExplosion(int x, int y);

	// destructor
	~CExplosion();

	// update the explosions
	void Update(float deltaTime, float startTime);

	// render the explosions
	void Render();

	// play the animation for explioding at a position
	void Explode(float x, float y);

	// where the code to move the explosion would go
	void Move(Direction direction, float deltaTime, float startTime);

	// check for collisions with other entities
	bool CheckForCollision(CWorldEntity *otherEntity);

private:
	// frames for the sprites for explosions
	const int START = 178;
	const int END = 193;
	const int ANIMATION_FRAME_CORRECTION = 1;
};
