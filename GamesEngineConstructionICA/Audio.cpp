#include "Audio.h"

CAudio CAudio::_audio;

CAudio::CAudio()
{
}


CAudio::~CAudio()
{
	// delete all audio fileinstances
	_files.clear();
}

void CAudio::Init()
{
}

int CAudio::CreateAudio(std::string const& filename)
{
	// local to hold the file id
	int id = 0;

	// create the audio file instance
	if (!HAPI->LoadSound(filename, &id)){
		HAPI->DebugText("Error: Failed to load sound");
		HAPI->UserMessage("Failed to load sound", "ERROR");
		HAPI->Close();
	};

	// add to vector
	_files.push_back(id);

	// return id
	return _files.size() - 1; 
	
}


void CAudio::Play(int id, bool loop, int volume, int pan)
{
	// play the specific audio file via HAPI
	HAPI->PlayASound(_files[id], loop, volume, pan);
}

void CAudio::PlayStream(std::string const& filename)
{
	HAPI->PlayStreamedMedia(filename, false);
}


void CAudio::StopPlay(int id)
{
	// stop the sound
	HAPI->StopSound(_files[id]);
}

void CAudio::StopStream()
{
	HAPI->StopStreamedMedia();
}