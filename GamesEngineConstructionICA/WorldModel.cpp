#include "WorldModel.h"

CWorldModel::CWorldModel()
{
}


CWorldModel::~CWorldModel()
{
}

void CWorldModel::HandleEvents()
{
	// read keyboard
	HAPI->GetKeyboardData(&_keyData);

	// read controller data
	if (HAPI->GetControllerData(0, &_controllerData))
	{
		// if successfull the must be plugged in
		SetControllerPresent(true);
	}
	else
	{
		SetControllerPresent(false);
	}

	// check for ESC key
	if (_keyData.scanCode[HK_ESCAPE])
	{
		// bail out
		SetRunning(false);
		CAudio::Instance()->StopStream();
		HAPI->Close();
	}
}

void CWorldModel::Initialise()
{
	// initialise Visualisation
	CVisualisation::Instance()->Init();

	// create initial background
	CBackground* background = new CBackground(0, 0);
	_entities.push_back(background);

	// create player
	CPlayer* player = new CPlayer(0, 321);
	_entities.push_back(player);

	// create bullet
	CBullet* bullet = new CBullet(0, 0);
	_entities.push_back(bullet);
	
	// create enemy
	for (int i = 0; i <= 40; i++)
	{
		CEnemy* enemy = new CEnemy(rand() % 2048 + 1069, rand() % 469);
		_entities.push_back(enemy);
	}

	//create explosion
	CExplosion* explosion = new CExplosion(0, 0);
	_entities.push_back(explosion);
}

void CWorldModel::Cleanup()
{
	// close the API
	CAudio::Instance()->StopStream();
	HAPI->Close();
}

void CWorldModel::Update(float deltaTime)
{
	//update HAPI
	HAPI->Update();
	if (!GetPlayer()->GetHealth() == 0)
	{
		// update the player
		if (_keyData.scanCode['A'] || _keyData.scanCode[HK_LEFT] || (IsControllerPresent() && _controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_X] <= CTRLMin() / 2))
		{
			if (GetPlayer()->GetDirection() != LEFT)
			{
				GetPlayer()->SetDirection(LEFT);
				GetPlayer()->SetDirectionChanged(true);
			}
			else
			{
				GetPlayer()->SetDirectionChanged(false);
			}
			GetPlayer()->Move(LEFT, deltaTime, (float)GetFrameStartTime());
		}

		if (_keyData.scanCode['D'] || _keyData.scanCode[HK_RIGHT] || (IsControllerPresent() && _controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_X] >= CTRLMax() / 2))
		{
			if (GetPlayer()->GetDirection() != RIGHT)
			{
				GetPlayer()->SetDirection(RIGHT);
				GetPlayer()->SetDirectionChanged(true);
			}
			else
			{
				GetPlayer()->SetDirectionChanged(false);
			}
			GetPlayer()->Move(RIGHT, deltaTime, (float)GetFrameStartTime());
		}

		if (_keyData.scanCode['W'] || _keyData.scanCode[HK_UP] || (IsControllerPresent() && _controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_Y] >= CTRLMax() / 2))
		{
			if (GetPlayer()->GetDirection() != UP)
			{
				GetPlayer()->SetDirection(UP);
			}
			else{}
			GetPlayer()->Move(UP, deltaTime, (float)GetFrameStartTime());
		}

		if (_keyData.scanCode['S'] || _keyData.scanCode[HK_DOWN] || (IsControllerPresent() && _controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_Y] <= CTRLMin() / 2))
		{
			if (GetPlayer()->GetDirection() != DOWN)
			{
				GetPlayer()->SetDirection(DOWN);
			}
			else{}
			GetPlayer()->Move(DOWN, deltaTime, (float)GetFrameStartTime());
		}

		if (_keyData.scanCode[HK_SPACE] || (IsControllerPresent() && _controllerData.analogueButtons[HK_ANALOGUE_RIGHT_TRIGGER]))
		{
			// fire a bullet from the players current position
			if (!GetBullet()->IsVisible())
			{
				GetBullet()->Fire(GetPlayer()->GetX() + 56, GetPlayer()->GetY() + 19);
			}
		}

		if ((_controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_X] > CTRLMin() / 2) && (_controllerData.analogueButtons[HK_ANALOGUE_LEFT_THUMB_X] < CTRLMax() / 2) && (!_keyData.scanCode['A'] && !_keyData.scanCode[HK_LEFT]) && (!_keyData.scanCode['D'] && !_keyData.scanCode[HK_RIGHT]))
		{
			GetPlayer()->SetMoving(false);
		}

		if (_keyData.scanCode[HK_ESCAPE])
		{
			//close HAPI and stop audio stream
			CAudio::Instance()->StopStream();
			HAPI->Close();
		}
		// loop through all entities and update
		for (size_t i = 0; i < _entities.size(); i++)
		{
			_entities[i]->Update(deltaTime, (float)GetFrameStartTime());
			CAI::Instance()->AILogic(GetPlayer(), _entities[i], deltaTime, (float)GetFrameStartTime());
			//check player hits asteroid

			if (_entities[i]->GetEntityType() == ENEMY)
			{
				if (GetPlayer()->CheckForCollision(_entities[i]))
				{
					GetPlayer()->LowerHealth(_entities[i]->GetDamage());
					CAudio::Instance()->Play(1, false, 1000, 500);
				}
				//Check the bullet hits asteroid
				if (GetBullet()->CheckForCollision(_entities[i]))
				{
					// hide enemy
					_entities[i]->SetVisible(false);

					// explode
					GetExplosion()->Explode(_entities[i]->GetX(), _entities[i]->GetY());
					// increment score
					_score += _entities[i]->GetScorePoints();
				}
				if (_entities[i]->GetX() < -80)
				{
					_entities[i]->SetVisible(false);
				}
				if (!_entities[i]->IsVisible())
				{
					_xOffset = rand() % 1024 + 1069;
					_yOffset = rand() % 469;

					_entities[i]->SetX(_xOffset);
					_entities[i]->SetY(_yOffset);
					_entities[i]->SetVisible(true);
				}
			}
			if (_entities[i]->GetEntityType() == BACKGROUND)
			{
				if (!_entities[i]->IsVisible())
				{
					GetBackground()->SetVisible(true);
				}
			}
		}
		//show the score and health of the player on the screen
		HAPI->RenderText(20, 10, HAPI_TColour(255, 255, 255), "SCORE : " + std::to_string(_score));
		HAPI->RenderText(20, 20, HAPI_TColour(255, 255, 255), "HEALTH : " + std::to_string(GetPlayer()->GetHealth()));
	}
	else if (GetPlayer()->GetHealth() == 0)
	{
		//change the gamestate
		SetGameState(GAME_OVER);
	}

	if (GetGameState() == GAME_OVER)
	{
		HAPI->RenderText(512, 256, HAPI_TColour(255, 255, 255), "You Died!");
		HAPI->RenderText(512, 266, HAPI_TColour(255, 255, 255), "SCORE : " + std::to_string(_score));
	}

}

void CWorldModel::Render()
{
	// loop through all entities and render
	for (size_t i = 0; i < _entities.size(); i++)
	{
		_entities[i]->Render();
	}
}

