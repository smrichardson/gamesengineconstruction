#pragma once
#include "GameEngine.h"
#include "GameState.h"
#include "PlayState.h"
#include "Player.h"
#include "Utils.h"
#include "Visualisation.h"
#include "Sprite.h"


//PlayState PlayState::_InitState;

void PlayState::Init()
{
	// create the initial background for the state
	//_backgroundSprite = CVisualisation::Instance()->CreateSprite("Monkey1.png", 1024, 512, 0, 0);
}

void PlayState::Cleanup()
{
	
}

void PlayState::Pause()
{

}

void PlayState::Resume()
{

}


// update the game state for this cycle
void PlayState::Update(int deltaTime)
{
	// update the player
	//GetPlayer()->Update();
}

// render the game state
void PlayState::Render()
{
	// refresh the background on the screen
	CVisualisation::Instance()->DrawBackground(14, 0, 0);
	
	// render the player state
	//game->GetPlayer()->Render();
}

void PlayState::HandleEvents()
{

}