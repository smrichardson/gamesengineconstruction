#pragma once
#ifndef GAMEENGINE_H
#define GAMEENGINE_H
#include <vector>
#include <HAPI_lib.h>
#include "Player.h"

class GameState;

class GameEngine
{
public:
	GameEngine();
	~GameEngine();
	void Init();
	void Cleanup();

	void ChangeState(GameState* state);
	void PushState(GameState* state);
	void PopState();

	void HandleEvents();
	void Update(int deltaTime);
	void Render();

	bool Running() { return _running; }
	void Quit() { _running = false; }


	DWORD FrameStartTime() { return _frameStartTime; }
	void SetFrameStartTime(const DWORD& value) { _frameStartTime = value; }
	DWORD FrameEndTime() { return _frameEndTime; }
	void SetFrameEndTime(const DWORD& value) { _frameEndTime = value; }
	DWORD GetFrameStartTime() { return _frameStartTime; }
	DWORD GetFrameEndTime() { return _frameEndTime; }
	//void SetDeltaTime(DWORD& value) { _deltaTime = value; }
	int GetDeltaTime(){ return _deltaTime; }
	int _deltaTime = 0;
	const int CTRLMax() { return _ctrlMax; }
	const int CTRLMin() { return _ctrlMin; }
	int GetFrameMs() { return FRAME_MS; }
	void SetRunning(const bool& value) { _running = value; }

	CPlayer* GetPlayer() { return _player; }

	// get the class instance
	static GameEngine* Instance()
	{
		return &_gameEngine;
	}

protected:

	// container for all potential game states
	std::vector<GameState*> _states;

private:


	//max values for the controller analogue sticks
	const int _ctrlMax = 32767;
	const int _ctrlMin = -32767;

	// frame rate constants
	const int FRAMES_PER_SECOND = 60;
	const int FRAME_MS = 1000 / FRAMES_PER_SECOND;

	// times used for delta time calculation
	DWORD _frameStartTime = 0;
	DWORD _frameEndTime = 0;

	CPlayer *_player = 0;
	bool _running;

	// the variable to hold the instance of this class
	static GameEngine _gameEngine;
};

#endif