#include "Enemy.h"
#include "Utils.h"
#include "Visualisation.h"
#include "WorldModel.h"

CEnemy::CEnemy()
{
}

CEnemy::CEnemy(int x, int y) : CWorldEntity(x, y, ENEMY)
{
	for (unsigned int i = 0; i < 4; i++)
		CVisualisation::Instance()->CreateSprite("asteroid_sprites1.png", 102, 85, 0, i);

	SetVisible(true);
	_scorePoints = 20;
	_damage = 10;
	EntityRect = CRectangle(25, 70, 30, 75);

	// different asteroid sprites
	//for (unsigned int i = 0; i < 4; i++)
	//	CVisualisation::Instance()->CreateSprite("asteroid_sprites1.png", 102, 85, 1, i);
}

CEnemy::~CEnemy()
{
}

void CEnemy::Update(float deltaTime, float startTime)
{
	if (!_moving) // not moving
	{
		_spriteFrame = IDLE;
	}
	else // moving
	{
		if (GetDirection() == UP)
		{
			if (DirectionChanged())
			{
				_spriteFrame = START_MOVE_LEFT;
			}
			if (_frameCount++ > ANIMATION_FRAME_CORRECTION)
			{
				if (_spriteFrame < END_MOVE_LEFT)
				{
					_spriteFrame++;
				}
				else
				{
					_spriteFrame = START_MOVE_LEFT;
				}
				_frameCount = 0;
			}
		}
		else if (GetDirection() == DOWN)
		{
			if (DirectionChanged())
			{
				_spriteFrame = START_MOVE_RIGHT;
			}
			if (_frameCount-- < ANIMATION_FRAME_CORRECTION)
			{
				if (_spriteFrame > END_MOVE_RIGHT)
				{
					_spriteFrame--;
				}
				else
				{
					_spriteFrame = START_MOVE_RIGHT;
				}
				_frameCount = 0;
			}
		}
	}
}

void CEnemy::Render()
{
	if (IsVisible())
	{
		CVisualisation::Instance()->DrawSprite(_spriteFrame, _posX, _posY, 0);
	}
}

void CEnemy::Move(Direction direction, float deltaTime, float startTime)
{
	SetMoving(true);
	switch (direction)
	{
	case RIGHT: // move the enemy right
		incrementX(_enemyIncrement, deltaTime, startTime);
		break;
	case LEFT: // move the enemy left
		decrementX(_enemyIncrement, deltaTime, startTime);
		break;
	case UP: // move the enemy up
		decrementY(_enemyIncrement, deltaTime, startTime);
		break;
	case DOWN: // move the enemy down
		incrementY(_enemyIncrement, deltaTime, startTime);
		break;
	}
}

bool CEnemy::CheckForCollision(CWorldEntity* otherEntity)
{
	return false;
}