#include "WorldEntity.h"
#include "Utils.h"

CWorldEntity::CWorldEntity()
{
}

CWorldEntity::CWorldEntity(int x, int y, EntityType entityType)
{
	// set intial position of entity
	_posX = x;
	_posY = y;
	_entityType = entityType;
}

CWorldEntity::~CWorldEntity()
{
}

// the lerp code is not quite right so it is not implemented yet
void CWorldEntity::incrementX(int x, float deltaTime, float startTime)
{
	int newPos = (x + _posX);
	/*_posX = (int)Utils::Lerp(_posX, newPos, deltaTime, startTime) * _posX;*/
	_posX = newPos;
}

void CWorldEntity::decrementX(int x, float deltaTime, float startTime)
{
	int newPos = _posX - x;
	/*_posX = (int)Utils::Lerp(_posX, newPos, deltaTime, startTime)* _posX;*/
	_posX = newPos;
}

void CWorldEntity::incrementY(int y, float deltaTime, float startTime)
{
	int newPos = y + _posY;
	/*_posY = (int)Utils::Lerp(_posX, newPos, deltaTime, startTime) * _posY;*/
	_posY = newPos;
}

void CWorldEntity::decrementY(int y, float deltaTime, float startTime)
{
	int newPos = _posY - y;
	//_posY = (int)Utils::Lerp(_posX, newPos, deltaTime, startTime) * _posY;
	_posY = newPos;
}