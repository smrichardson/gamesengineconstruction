#pragma once
#ifndef WORLDENTITY_H
#define WORLDENTITY_H

#include "Rectangle.h"

// directions the entity can travel
enum Direction {
	RIGHT = 0,
	LEFT,
	UP,
	DOWN
};

//types of entity
enum EntityType
{
	PLAYER,
	BULLET,
	ENEMY,
	BACKGROUND,
	EXPLOSION
};

class CWorldEntity
{
public:

	// default constructor
	CWorldEntity();

	// Create an entity instance at the specific screen position
	//
	// parameters:
	// x - the x position of the entity on the screen
	// y - the y position of the entity on the screen
	// entityType - the type of the entity
	//
	CWorldEntity(int x, int y, EntityType entityType);

	// destructor
	~CWorldEntity();

	// handle update of entity
	virtual void Update(float deltaTime, float startTime) = 0;
	// handle render of entity
	virtual void Render() = 0;
	// moves the position of the entity
	virtual void Move(Direction direction, float deltaTime, float startTime) = 0;
	// check for the collisions
	virtual bool CWorldEntity::CheckForCollision(CWorldEntity* otherEntity) = 0;

	// get the X position of the entity
	int GetX() { return _posX; }
	// set the X position of the entity
	void SetX(int& value) { _posX = value; }
	// get the Y position of the entity
	int GetY() { return _posY; }
	// set the Y position of the entity
	void SetY(int& value) { _posY = value; }

	// increment the X position of the entity
	void incrementX(int x, float deltaTime, float startTime);
	// decrement the X position of the entity
	void decrementX(int x, float deltaTime, float startTime);
	// increment the Y position of the entity
	void incrementY(int y, float deltaTime, float startTime);
	// decrement the Y position of the entity
	void decrementY(int y, float deltaTime, float startTime);

	// check if entity is visible
	bool IsVisible() { return _visible; }

	// set the visibility of the entity
	void SetVisible(bool value) { _visible = value; }

	//get the entity direction
	int GetDirection(){ return _direction; }
	// get the entity direction
	void SetDirection(int value){ _direction = value; }

	// get the entity speed
	int GetSpeed(){ return _speed; }

	// check if entity direction changed
	bool DirectionChanged() { return _directionChanged; }
	// set direction changed
	void SetDirectionChanged(bool value) { _directionChanged = value; }

	// check if entity moving
	bool IsMoving() { return _moving; }
	// set entity moving
	void SetMoving(bool value) { _moving = value; }

	//Get the entityType of the entity
	EntityType GetEntityType() { return _entityType; }

	//Dimensions of the rectangle to collide with other entity
	CRectangle GetRect() { return EntityRect; }

	// get the score
	int GetScorePoints() { return _scorePoints; }
	//get the health
	int GetHealth() { return _health; }
	//get the damage the entity can deal
	int GetDamage() { return _damage; }
	//lower the health of an entity
	void LowerHealth(int value) { _health = _health - value; }

protected:

	bool _directionChanged = false; // indicates that the entities direction has changed
	int _speed = 0; // the speed that the entity is moving
	int _posX; // the X position of the entity on the screen
	int _posY; // the Y position of the entity on the screen
	int _direction = UP; // the current direction that the entity is moving in
	int _spriteFrame = 0; // the frame to render
	int _frameCount = 0; // counter used to delay frame animation
	bool _moving = false; // indicates if the entity is moving
	bool _visible = false; // indicates the entities visibility
	int _scorePoints; // indicates the score of the entity
	int _health; // the health of the entity
	int _damage; // the damage the entity deals
	EntityType _entityType;//type of the entity
	CRectangle EntityRect = CRectangle(top, bottom, left, right);// the rectangle around the entity
private:
	int top, bottom, left, right; // dimensions of the rectangle
};

#endif
