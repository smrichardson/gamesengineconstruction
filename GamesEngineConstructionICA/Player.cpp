#pragma once
#include "Player.h"
#include "Utils.h"
#include "Visualisation.h"
#include "Bullet.h"
#include "Audio.h"
#include "WorldModel.h"

CPlayer::CPlayer()
{
}

CPlayer::CPlayer(int x, int y) : CWorldEntity(x, y, PLAYER)
{
	_health = 1000;

	CVisualisation::Instance()->CreateSprite("Arcade - Gradius IV Fukkatsu - Vic Viper.png", 58, 47, 0, 5);

	for (unsigned int i = 1; i < 5; i++)
		CVisualisation::Instance()->CreateSprite("Arcade - Gradius IV Fukkatsu - Vic Viper.png", 58, 47, 0, i);

	for (unsigned int i = 6; i < 10; i++)
		CVisualisation::Instance()->CreateSprite("Arcade - Gradius IV Fukkatsu - Vic Viper.png", 58, 47, 0, i);
	
	// an extra animation for the player
	/*for (unsigned int i = 10; i < 13; i++)
		CVisualisation::Instance()->CreateSprite("Arcade - Gradius IV Fukkatsu - Vic Viper.png", 23, 8, 8, i);*/

	SetVisible(true);
	EntityRect = CRectangle(8, 34, 3, 40);
}

CPlayer::~CPlayer()
{

}


void CPlayer::Update(float deltaTime, float startTime)
{
	if (!_moving) // not moving
	{
		_spriteFrame = IDLE;
	}
	else // moving
	{
		if (GetDirection() == UP)
		{
			if (_frameCount++ > ANIMATION_FRAME_CORRECTION)
			{
				if (_spriteFrame < END_MOVE_UP)
				{
					_spriteFrame++;
				}
				else
				{
					
				}
				_frameCount = 0;
			}
		}
		else if (GetDirection() == DOWN)
		{
			if (_frameCount-- < ANIMATION_FRAME_CORRECTION)
			{
				if (_spriteFrame > END_MOVE_DOWN)
				{
					_spriteFrame--;
				}
				else
				{
					
				}
				_frameCount = 0;
			}
		}
	}
}

void CPlayer::Render()
{
	CVisualisation::Instance()->DrawSprite(_spriteFrame, _posX, _posY, 0);
}

void CPlayer::Move(Direction direction, float deltaTime, float startTime)
{
	SetMoving(true);
	switch (direction)
	{
	case RIGHT: // move the player right on the screen
		incrementX(_playerIncrement, deltaTime, startTime);
		break;
	case LEFT: // move the player left on the screen
		decrementX(_playerIncrement, deltaTime, startTime);
		break;
	case UP: // move the player up on the screen
		decrementY(_playerIncrement, deltaTime, startTime);
		break;
	case DOWN: // move the player down on the screen
		incrementY(_playerIncrement, deltaTime, startTime);
		break;
	}
}

bool CPlayer::CheckForCollision(CWorldEntity* otherEntity)
{
	if ((GetX() + EntityRect.right > otherEntity->GetX() + otherEntity->GetRect().left)
		&& (GetX() < otherEntity->GetX() + otherEntity->GetRect().right)
		&& (GetY() + EntityRect.bottom > otherEntity->GetY() + otherEntity->GetRect().top)
		&& (GetY() < otherEntity->GetY() + otherEntity->GetRect().bottom))
	{
		if (GetHealth() <= 0)
		{
			SetVisible(false);
		}
		else
		{

		}
		return true;
	}
	else
	{
		return false;
	}
}