#include "Sprite.h"
#include <stdlib.h>
#include "Utils.h"
#include "Visualisation.h"

// Default constructor
CSprite::CSprite()
{
}

// Destructor
CSprite::~CSprite()
{
}

// Constructor
CSprite::CSprite(std::string const& filename, int width, int height, int row, int col)
{
	// set the sprite dimensions
	_width = width;
	_height = height;
	_row = row;
	_col = col;

	// load the png file from hapi into filename
	// if file loaded successfully then
	if (!HAPI->LoadTexture(filename, &_imageFile, &_imageFileWidth, &_imageFileHeight)){
		HAPI->DebugText("Error: Failed to load image");
		HAPI->UserMessage("Failed to load image", "ERROR");
		HAPI->Close();
	};
}

// Render the sprite clipped
void CSprite::Render(BYTE* screen, int screenHeight, int screenWidth, int x, int y, int frameNumber)
{
	CRectangle destRect(0, screenHeight, 0, screenWidth);
	CRectangle srcRect(0, _imageFileHeight, 0, _imageFileWidth);
	Utils::CropBlit(screen, destRect, _imageFile, srcRect, x, y, _row, _col, _width, _height);
}

// Render the sprite full screen
void CSprite::Render(BYTE* screen, int screenHeight, int screenWidth, int x, int y)
{
	CRectangle destRect(0,screenHeight, 0 ,screenWidth);
	CRectangle srcRect(0, _imageFileHeight, 0, _imageFileWidth);
	Utils::ClipBlit(screen, destRect, _imageFile, srcRect, x, y);
}
