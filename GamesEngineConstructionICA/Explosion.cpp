#include "Explosion.h"
#include "Visualisation.h"
#include "Audio.h"

CExplosion::CExplosion()
{
}

CExplosion::CExplosion(int x, int y) : CWorldEntity(x, y, EXPLOSION)
{
	// initialize all of the sprites needed
	for (int j = 0; j < 5; j++)
	{
		for (int i = 0; i < 5; i++)
		{
			CVisualisation::Instance()->CreateSprite("expl-composite.png", 64, 64, j, i);
		}
	}
	SetVisible(false);
	CAudio::Instance()->CreateAudio("explodemini.wav");
}

CExplosion::~CExplosion()
{
}

void CExplosion::Update(float deltaTime, float startTime)
{
	if (_frameCount++ > ANIMATION_FRAME_CORRECTION)
	{
		if (_spriteFrame < END)
		{
			_spriteFrame++;
		}
		else
		{
			_visible = false;
		}
		_frameCount = 0;
	}
}

void CExplosion::Render()
{
	// only render the sprite if visible
	if (_visible == true)
	{
		CVisualisation::Instance()->DrawSprite(_spriteFrame, _posX, _posY, 0);
	}
}

void CExplosion::Move(Direction direction, float deltaTime, float startTime)
{

}

void CExplosion::Explode(float x, float y)
{
	// set the initial position of the explosion
	_posX = x;
	_posY = y;

	// make it visible
	_visible = true;

	// play its sound
	CAudio::Instance()->Play(1, false, 1000, 500);

	// set start sprite
	_spriteFrame = START;
}

bool CExplosion::CheckForCollision(CWorldEntity* otherEntity)
{
	return(false);
}