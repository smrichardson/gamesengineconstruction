#pragma once
#ifndef ENEMY_H
#define ENEMY_H


#include "HAPI_lib.h"
#include "WorldEntity.h"


class CEnemy : public CWorldEntity
{
public:

	CEnemy();
	CEnemy(int x, int y);
	~CEnemy();

	void Update(float deltaTime, float startTime);
	void Render();
	void Move(Direction direction, float deltaTime, float startTime);
	bool CWorldEntity::CheckForCollision(CWorldEntity* otherEntity);

private:
	int _health;
	int _enemyIncrement = 6;

	// constants for animation
	const int IDLE = 14;
	//const int IDLE_LEFT = 4;
	const int START_MOVE_LEFT = 14;
	const int END_MOVE_LEFT = 17;
	const int START_MOVE_RIGHT = 14;
	const int END_MOVE_RIGHT = 17;
	const int ANIMATION_FRAME_CORRECTION = 12;
};
#endif
