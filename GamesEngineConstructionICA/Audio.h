#pragma once
#ifndef AUDIO_H
#define AUDIO_H
#include <HAPI_lib.h>
#include <vector>
#include "Sprite.h"


class CAudio
{
public:
	// Default constructor
	CAudio();

	// Destructor
	~CAudio();
	
	// Class initialiser
	void Init();
	
	// Create a sprite instance
	//
	// parameters:
	// filename - the name of the audio file
	//
	// returns : an integer representation of the audio file
	//
	int CreateAudio(std::string const& filename);

	// Play a specific audio file
	//
	// parameters:
	// id - the id of the file to play
	void Play(int id, bool loop, int volume, int pan);

	// Stop a sound from playing
	//
	// parameters:
	// id - the id of the file to stop
	void StopPlay(int id);

	// get the class instance
	static CAudio* Instance()
	{
		return &_audio;
	}
	void PlayStream(std::string const& filename);
	void StopStream();

private:

	// audio file instances
	std::vector<int> _files;

	// the variable to hold the instance of this class
	static CAudio _audio;
};

#endif