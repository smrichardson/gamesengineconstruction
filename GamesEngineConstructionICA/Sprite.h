#pragma once
#include "HAPI_lib.h"

class CVisualisation;

class CSprite
{
public:
	// Default constructor
	CSprite();

	// Constructor
	//
	// parameters:
	//
	// filename - the sprite texture filename
	// width - the width of the sprite image
	// height - the height of the sprite image
	// row - the row in the texture file that holds the image
	// col - the column in the texture file that holds the image
	//
	CSprite(std::string const& filename, int width, int height, int row, int col);

	//Destructor
	~CSprite();

	// render the sprite
	//
	// parameters:
	//
	// screen - the screen we're writing to
	// screenHeight - the height of the screen in pixels
	// screenWidth - the width of the screen in pixels
	// x - the x position on the screen to render to
	// y - the y position on the screen to render to
	// frameNumber - the number of the frame to render
	//
	void Render(BYTE* screen, int screenHeight, int screenWidth, int x, int y, int frameNumber);

	// render the sprite
	//
	// parameters:
	//
	// screen - the screen we're writing to
	// screenHeight - the height of the screen in pixels
	// screenWidth - the width of the screen in pixels
	//
	void Render(BYTE* screen, int screenHeight, int screenWidth, int x, int y);

	// get the data source
	BYTE* GetImageFile(){ return _imageFile; }

private:
	int _width = 0;   // the width of a single sprite image � e.g. 104
	int _height = 0;  // the height of a single sprite image � e.g. 150
	int _row = 0;     // the row of the sprite image in the texture file
	int _col = 0;     // the column of the sprite image in the texture file
	
	BYTE* _imageFile = 0; // the texture file data
	int  _imageFileHeight = 0; // the height of the texture file
	int _imageFileWidth = 0;   // the width of the texture file
};

