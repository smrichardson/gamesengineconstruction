#pragma once
#ifndef AI_H
#define AI_H

#include <HAPI_lib.h>
#include "WorldEntity.h"

class CAI
{
public:
	// Default constructor
	CAI();

	// Destructor
	~CAI();

	// Play a specific audio file
	//
	// parameters:
	// id - the id of the file to play
	void AILogic(CWorldEntity* entity, CWorldEntity* otherEntity, float deltaTime, float startTime);

	// get the class instance
	static CAI* Instance()
	{
		return &_AI;
	}

private:
	// the variable to hold the instance of this class
	static CAI _AI;

	// data used for finding the angles between entities
	double PI = 3.14159265359;

	double _hypDist;
	int _distX;
	int _distY;

	double _angleX;
	double _angleY;
};

#endif