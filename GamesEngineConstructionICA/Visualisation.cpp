#include "Visualisation.h"
#include "Sprite.h"
#include "Utils.h"

CVisualisation CVisualisation::_visualisation;

CVisualisation::CVisualisation()
{
}


CVisualisation::~CVisualisation()
{
	// delete all sprite instances
	_sprites.clear();
}

void CVisualisation::Init()
{
	// initialise the HAPI engine
	if (!HAPI->Initialise(&_screenWidth, &_screenHeight))
		return;

	// display frame rate on screen
	HAPI->SetShowFPS(true);

	// sets the screen
	_screen = HAPI->GetScreenPointer();
}

int CVisualisation::CreateSprite(std::string const& filename, int width, int height, int row, int col)
{
	// create the sprite instance
	CSprite* sprite = new CSprite(filename, width, height, row, col);

	// add to vector
	_sprites.push_back(sprite);

	// return id
	return _sprites.size() - 1;
}


void CVisualisation::ClearScreen()
{

}

void CVisualisation::DrawSprite(int spriteId, int x, int y, int frameNumber)
{
	// get the sprite from the collection
	// and render it
	_sprites[spriteId]->Render(_screen, _screenHeight, _screenWidth, x, y, frameNumber);
}

void CVisualisation::DrawBackground(int spriteId, int x, int y)
{
	// get the sprite from the collection
	// and render it
	_sprites[spriteId]->Render(_screen, _screenHeight, _screenWidth, x , y);
}